var express = require('express');
var server;
module.exports = server = express();

require('./settings')(server);    // Load settings
require('./models')(server);      // Load models + connect to the DB

console.log('Server listening on port', server.settings.port);
server.listen(server.settings.port); // Start the server