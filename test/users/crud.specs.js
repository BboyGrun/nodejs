var chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var CONSTANTS = require('../constants.json');

var app = require('../../server.js');

describe('Users', function() {
    beforeEach(function loadingFixtures(done) {
        //fixtures.load();
        done();
    });

    describe('create (/2 points)', function() {
        it('POST /users without email or password should return a 400 BAD REQUEST', function(done) {
            return chai.request(app)
            .post('/users')
            .set({'Content-type': 'application/json'})
            .send({password: 'password'})
            .end(function(res) {
                expect(res).to.have.status(400);
                done();
            });
        });

        it('POST /users should create a new user', function(done) {
            return chai.request(app)
            .post('/users')
            .set({'Content-type': 'application/json'})
            .send({email: 'foo@bar.com', password: 'password'})
            .end(function(res) {
                expect(res).to.have.status(200);
                done();
            });
        });
    });

    describe('update (/2 points)', function() {
        it('PUT /users/:id with an invalid userId should return a 404 NOT FOUND', function(done) {
            return chai.request(app)
            .put('/users/' + CONSTANTS.UNKOWNUSER)
            .set({'Content-type': 'application/json'})
            .send({password: 'password'})
            .end(function(res) {
                expect(res).to.have.status(404);
                done();
            });
        });

        it('PUT /users/:id should update the user', function(done) {
            return chai.request(app)
            .put('/users/' + CONSTANTS.USERID)
            .set({'Content-type': 'application/json'})
            .send({password: 'password'})
            .end(function(res) {
                expect(res).to.have.status(200);
                done();
            });
        });
    });

    describe('list (/1 point)', function() {
        it('GET /users/ should list all the users', function(done) {
            return chai.request(app)
            .get('/users')
            .send()
            .end(function(res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.instanceof('Array')
                expect(res.body.length).to.equal(1)
                done();
            })
        });
    });

    describe('show (/2 points)', function() {
        it('GET /users/:id with an invalid ID should return 404 NOT FOUND', function(done) {
            return chai.request(app)
            .get('/users/' + CONSTANTS.USER.UNKNOWN_ID)
            .send()
            .end(function(res) {
                expect(res).to.have.status(404);
                done();
            })
        });
        
        it('GET /users/:id with a valid ID should return the correct user', function(done) {
            return chai.request(app)
            .get('/users/' + CONSTANTS.USER.KNOWN_ID)
            .send()
            .end(function(res) {
                expect(res).to.have.status(200);
                done();
            })
        });
    });
});
