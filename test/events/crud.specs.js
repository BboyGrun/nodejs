var chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var CONSTANTS = require('../constants.json');
var app = require('../../server.js');

describe('Events', function() {
    beforeEach(function loadingFixtures(done) {
        //fixtures.load();
        done();
    });

    describe('create (/2 points)', function() {
        it('POST /events without authentication should return a 401 UNAUTHORIZED', function(done) {
            return chai.request(app)
            .post('/events')
            .set({'Content-type': 'application/json'})
            .send({title: 'event'})
            .end(function(res) {
                expect(res).to.have.status(401);
                done();
            });
        });

        it('POST /events should create a new event when the user is authenticated', function(done) {
            return chai.request(app)
            .post('/events')
            .set({'Authorization': CONSTANTS.ACCESS_TOKEN})
            .set({'Content-type': 'application/json'})
            .send({title: 'event'})
            .end(function(res) {
                expect(res).to.have.status(200);
                expect(res.body._id).to.not.be.undefined;
                expect(res.body.title).to.equal('event');
                done();
            });
        });
    });

    describe('list (/1 point)', function() {
        it('GET /events/ should list all the events', function(done) {
            return chai.request(app)
            .get('/events')
            .send()
            .end(function(res) {
                expect(res).to.have.status(200);
                expect(res.body.length).to.equal(3);
                done();
            });
        });
    });

    describe('show (/2 points)', function() {
        it('GET /events/:id should return the correct user', function(done) {
            return chai.request(app)
            .get('/events/' + CONSTANTS.USERID)
            .send()
            .end(function(res) {
                expect(res).to.have.status(200);
                done();
            });
        });

        it('GET /events/:id with an invalid ID should return 404 NOT FOUND', function(done) {
            return chai.request(app)
            .get('/events/' + CONSTANTS.UNKOWNUSER)
            .send()
            .end(function(res) {
                expect(res).to.have.status(404);
                done();
            });
        });
    });
});
