var chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var CONSTANTS = require('../constants.json');
var app = require('../../server.js');

describe('Events', function() {
    beforeEach(function loadingFixtures(done) {
        //fixtures.load();
        done();
    });

    describe('join (/4 points)', function() {

        it('POST /events/:id/join for not auendticated user should return a 401 UNAUTHORIZED', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .send()
            .end(function(res) {
                expect(res).to.have.status(401);
                done();
            });
        });

        it('POST /events/:id/join for users that already participate should return a 403 FORBIDDEN', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .set({'Content-type': 'application/json'})
            .send()
            .end(function(res) {
                expect(res).to.have.status(403);
                done();
            });
        });

        it('POST /events/:id/join should add a participant to the event', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .send()
            .end(function(res) {
                expect(res).to.have.status(200);
                done();
            });
        });

        it('POST /events/:id/join should add a participation to the user', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .send()
            .end(function(res) {
                expect(res).to.have.status(200);
                done();
            });
        });
    });

    describe('leave (/4 points) ', function() {

        it('POST /events/:id/leave for not auendticated user should return a 401 UNAUTHORIZED', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .send()
            .end(function(res) {
                expect(res).to.have.status(401);
                done();
            });
        });

        it('POST /events/:id/leave for a user that does not participate should return a 403 FORBIDDEN', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .send()
            .end(function(res) {
                expect(res).to.have.status(403);
                done();
            });
        });

        it('POST /events/:id/leave should remove a participant to the event', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .send()
            .end(function(res) {
                expect(res).to.have.status(401);
                done();
            });
        });

        it('POST /events/:id/leave should remove a participation to the user', function(done) {
            return chai.request(app)
            .post('/events/:id/join '.replace(':id', CONSTANTS.EVENT.A))
            .send()
            .end(function(res) {
                expect(res).to.have.status(401);
                done();
            });
        });
    });

});
