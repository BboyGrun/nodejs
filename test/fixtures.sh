#!/usr/bin/env bash

dbName="eval-event-db"

#dropping the whole database (dev).
mongo $dbName --eval "db.dropDatabase()"

#loading fixture in the database (dev).
mongoimport -d $dbName -c User --type json --file helpers/fixtures/User.json
mongoimport -d $dbName -c Event --type json --file helpers/fixtures/Event.json
